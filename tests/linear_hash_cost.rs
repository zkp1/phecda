use rand::Rng;
use rustzk::field::*;
use rustzk::hash::*;
use rustzk::tape::*;
use std::time::Instant;


fn dummy_vith<T: Field>(comp_ratio: usize, ell: usize) {
    println!("comp: {}, ell: {}", comp_ratio, ell);

    let coset = Coset::<T>::init(6, 5, 1);

    let n = 64;
    let m = n / 2;
    let tape_len = 2 * ell + 1;

    let log_n = 6;
    let log_m = 5;

    let n_party = 16;

    let mut x: Vec<T> = vec![T::from(0); ell * m];
    let mut x_masked: Vec<T> = vec![T::from(0); ell * m];
    let mut a: Vec<Vec<T>> = vec![vec![T::from(0); ell * m]; comp_ratio];
    let mut y: Vec<T> = vec![T::from(0); comp_ratio];
    for i in 0..(ell * m) {
        x[i] = T::random();
    }
    for k in 0..comp_ratio {
        for i in 0..(ell * m) {
            a[k][i] = T::random();
        }
    }

    let mseed = Seed::random();
    let mut ptime: usize = 0;
    let mut ptime_hash: usize = 0;

let start = Instant::now();
    let mut st = SeedTree::new(&mseed, n_party, n);
    let mut tapes: Vec<Tape> = vec![];
    for i in 0..n {
        for p in 0..n_party {
            let seed = st.get_seed(i, p);
            // println!("{:?}", seed);
            tapes.push(Tape::new(seed, tape_len * T::SIZE));
        }
    }

    let mut vith_uu: Vec<T> = vec![T::from(0); tape_len * n];
    let mut vith_u0: Vec<T> = vec![T::from(0); tape_len * m];
    let mut vith_c: Vec<T> = vec![T::from(0); tape_len * (n - m)];
    let mut vith_v0: Vec<T> = vec![T::from(0); tape_len * n];

    for i in 0..tape_len {
        // compute u0, v0
        for j in 0..n {
            for k in 0..n_party {
                let t = tapes[j * n_party + k].get::<T>();
                // if i == tape_len / 2 && j < 2 && k != 0 {
                //     println!("P tape{} party{}: {:?}", j, k, t);
                // }
                vith_uu[i * n + j] += t;
                if j < m {
                    vith_u0[i * m + j] += t;
                } else {
                    vith_c[i * (n - m) + (j - m)] += t;
                }
                // TODO configurable set S,
                // here S = {1, 2, 3, ..., N}
                vith_v0[i * n + j] += T::from(k + 1) * t;
            }
        }
        // compute correction
        let tmp = (coset.fft)(
            &coset, &(coset.ifft)(
                &coset,
                &vith_u0[i * m..(i + 1) * m].to_vec(),
                log_m), log_m, log_n);
        for j in 0..m {
            assert_eq!(tmp[j], vith_u0[i * m + j]);
        }
        for j in m..n {
            vith_c[i * (n - m) + j - m] -= tmp[j];
        }
    }
    let root = st.commit();

let elapsed = start.elapsed();
let ms = elapsed.as_micros() as usize;
ptime += ms;
println!("\tprover time (init): {}", (ms as f64) / 1000.0);

    let h = T::random();
    // let mut hq: Vec<T> = vec![T::from(0); n];

let start = Instant::now();
    let mut hu: Vec<T> = vec![T::from(0); m];
    let mut hv: Vec<T> = vec![T::from(0); n];
    for i in 0..m {
        hu[i] = vith_u0[i];
        for j in 1..tape_len {
            hu[i] = hu[i] * h + vith_u0[i + j * m];
        }
    }

    for i in 0..n {
        hv[i] = vith_v0[i];
        for j in 1..tape_len {
            hv[i] = hv[i] * h + vith_v0[i + j * n];
        }
    }

    let mut hhv = SHA256::from(0);
    hhv.commit(&hv);

let elapsed = start.elapsed();
let ms = elapsed.as_micros() as usize;
ptime += ms;
ptime_hash = ms;
println!("\tprover time (hash): {}", (ms as f64) / 1000.0);


let start = Instant::now();
    for i in 0..(ell * m) {
        x_masked[i] = x[i] - vith_u0[i];
    }

    for k in 0..comp_ratio {
        for i in 0..(ell * m) {
            y[k] += a[k][i] * vith_u0[i + ell * m];
        }
    }
    let mut hy = SHA256::from(0);
    hy.commit(&y);

let elapsed = start.elapsed();
let ms = elapsed.as_micros() as usize;
ptime += ms;
println!("\tprover time (comp): {}", (ms as f64) / 1000.0);


    let alpha = T::random();


let start = Instant::now();

    let mut vith_s: Vec<T> = vec![T::from(0); ell * m];
    let mut vith_sv: Vec<T> = vec![T::from(0); ell * n];
    for i in 0..(ell * m) {
        vith_s[i] = alpha * vith_u0[i] + vith_u0[i + ell * m];
    }
    for i in 0..(ell * n) {
        vith_sv[i] = alpha * vith_v0[i] + vith_v0[i + ell * n];
    }
    let mut hsv = SHA256::from(0);
    hsv.commit(&vith_sv);

let elapsed = start.elapsed();
let ms = elapsed.as_micros() as usize;
ptime += ms;
println!("\tprover time (vith): {}", (ms as f64) / 1000.0);


    let missings = vec![13];

let start = Instant::now();

    let mut sto = SeedTreeOpen::new(&st);
    sto.missings = missings;
    st.open(&mut sto);

let elapsed = start.elapsed();
let ms = elapsed.as_micros() as usize;
ptime += ms;
println!("\tprover time (open): {}", (ms as f64) / 1000.0);

    println!("linear hash: {:.2} %", (ptime_hash as f64) / (ptime as f64) * 100.0);
    println!("===================================================");
}


fn linear_hash_cost<T: Field>(ell: usize) {
    // ell == tape_len
    let mut coset = Coset::<T>::init(6, 5, 1);
    // coset.precompute(33, true);

    println!("ell: {}", ell);

    let n = 64;
    let m = n / 2;
    let ell = ell + 1;
    let mut q: Vec<T> = vec![T::from(0); ell * n];
    let mut u: Vec<T> = vec![T::from(0); ell * m];
    let mut v: Vec<T> = vec![T::from(0); ell * n];
    // let mut h: Vec<T> = vec![T::from(0); n];
    let h = T::random();
    let mut d: Vec<T> = vec![T::from(0); n];

    let mut hu: Vec<T> = vec![T::from(0); m];
    let mut hv: Vec<T> = vec![T::from(0); n];
    let mut hq: Vec<T> = vec![T::from(0); n];

    for i in 0..(ell * n) {
        v[i] = T::random();
        q[i] = T::random();
    }
    for i in 0..(ell * m) {
        u[i] = T::random();
    }
    for i in 0..n {
        d[i] = T::random();
    }

    // prover time
    let start = Instant::now();

    for i in 0..m {
        hu[i] = u[i];
        for j in 1..ell {
            hu[i] = hu[i] * h + u[i + j * m];
        }
    }

    for i in 0..n {
        hv[i] = v[i];
        for j in 1..ell {
            hv[i] = hv[i] * h + v[i + j * n];
        }
    }

    let mut hhv = SHA256::from(0);
    hhv.commit(&hv);

    let elapsed = start.elapsed();
    let ms = elapsed.as_micros() as usize;
    println!("\tprover time: {}", (ms as f64) / 1000.0);

    // verifier time
    let start = Instant::now();

    for i in 0..n {
        hq[i] = q[i];
        for j in 1..ell {
            hq[i] = hq[i] * h + q[i + j * n];
        }
    }


    let tmp = (coset.fft)(
        &coset,
        &(coset.ifft)(&coset, &hu.to_vec(), 5),
        5, 6);
    for j in 0..n {
        hq[j] = hq[j] - tmp[j] * d[j];
    }
    let mut hhq = SHA256::from(0);
    hhq.commit(&hq);


    let elapsed = start.elapsed();
    let ms = elapsed.as_micros() as usize;
    println!("\tverifier time: {}", (ms as f64) / 1000.0);
}

#[test]
fn test() {
    println!("");
    for p in 1..16 {
        dummy_vith::<GF2p256>(1, 1 << p);
    }

    // println!("");
    // linear_hash_cost::<GF2p256>(54);    // 2 ^ 3
    // linear_hash_cost::<GF2p256>(56);    // 2 ^ 4
    // linear_hash_cost::<GF2p256>(60);    // 2 ^ 5
    // linear_hash_cost::<GF2p256>(64);    // 2 ^ 6
    // linear_hash_cost::<GF2p256>(68);    // 2 ^ 7
    // linear_hash_cost::<GF2p256>(72);    // 2 ^ 8
    // linear_hash_cost::<GF2p256>(76);    // 2 ^ 9
    // linear_hash_cost::<GF2p256>(78);    // 2 ^ 10
    // linear_hash_cost::<GF2p256>(82);    // 2 ^ 11
    // linear_hash_cost::<GF2p256>(86);    // 2 ^ 12
}
